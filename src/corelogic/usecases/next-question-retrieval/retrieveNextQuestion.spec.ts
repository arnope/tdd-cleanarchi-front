import {retrieveNextQuestion} from "./retrieveNextQuestion";
import {InMemoryQuestionGateway} from "../../../adapters/secondary/gateways/inMemoryQuestionGateway";
import {initReduxStore, ReduxStore} from "../../store/buildReduxStore";
import {Question} from "../../models/question";
import {AppState} from "../../store/appState";

describe('Next question retrieval', () => {

    let store: ReduxStore;
    let questionGateway: InMemoryQuestionGateway;
    let initialState: AppState;

    beforeEach(() => {
        questionGateway = new InMemoryQuestionGateway();
        store = initReduxStore({questionGateway});
        initialState = store.getState();
    });

    it('should not retrieve any question if there is no one available', async () => {
        await store.dispatch(retrieveNextQuestion());
        expectNextQuestion(null);
    });

    it('should retrieve the next question with available ones', async () => {
        const nextQuestion = {
            id: '123abc',
            label: "Qu'est-ce que le TDD ?",
            choices: {
                A: 'Toto-Driven Development',
                B: 'Trump-Driven Development',
                C: 'Test-Dodo Development',
                D: 'Test-Driven Development',
            }
        };
        questionGateway.nextQuestion = nextQuestion;
        await store.dispatch(retrieveNextQuestion());
        expectNextQuestion(nextQuestion);
    });

    const expectNextQuestion = (expectedNextQuestion: Question | null) => {
        expect(store.getState()).toEqual({
            ...initialState,
            nextQuestionRetrieval: {
                data: expectedNextQuestion
            }
        })
    }

});