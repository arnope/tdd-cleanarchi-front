import {createAsyncThunk} from "@reduxjs/toolkit";
import {QuestionGateway} from "../../gateways/questionGateway";
import {Question} from "../../models/question";

export const retrieveNextQuestion = createAsyncThunk<{ nextQuestion: Question | null },
    void,
    {
        extra: { questionGateway: QuestionGateway }
    }>(
    'questions/retrieveNextQuestion',
    async (_: void, {
        dispatch,
        extra: {questionGateway}
    }) => {
        const nextQuestion = await questionGateway.retrieveNextOne();
        return {nextQuestion};
    });