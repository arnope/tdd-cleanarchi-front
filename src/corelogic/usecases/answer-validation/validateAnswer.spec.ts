import {initReduxStore, ReduxStore} from "../../store/buildReduxStore";
import {validateAnswer} from "./validateAnswer";
import {AppState} from "../../store/appState";
import {InMemoryQuestionGateway} from "../../../adapters/secondary/gateways/inMemoryQuestionGateway";

describe('Answer live validation', () => {

    let store: ReduxStore;
    let initialState: AppState;
    let questionGateway: InMemoryQuestionGateway;

    beforeEach(() => {
        questionGateway = new InMemoryQuestionGateway();
        store = initReduxStore({questionGateway});
        store.dispatch({
            type: 'questions/retrieveNextQuestion/fulfilled', payload: {
                nextQuestion: {
                    id: '123abc',
                    label: "Qu'est-ce que le TDD ?",
                    choices: {
                        A: 'Toto-Driven Development',
                        B: 'Trump-Driven Development',
                        C: 'Test-Dodo Development',
                        D: 'Test-Driven Development',
                    }
                }
            }
        });
        initialState = store.getState();
    });

    it('should valid a good answer', async () => {
        await store.dispatch(validateAnswer('D'));
        expect(store.getState()).toEqual({
            ...initialState,
            answerValidation: {
                '123abc': {
                    goodAnswer: 'D',
                    givenAnswer: 'D',
                }
            }
        });
    });

})
;