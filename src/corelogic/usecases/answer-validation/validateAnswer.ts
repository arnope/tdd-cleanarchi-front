import {createAsyncThunk} from "@reduxjs/toolkit";
import {AppState} from "../../store/appState";
import {QuestionGateway} from "../../gateways/questionGateway";

export const validateAnswer = createAsyncThunk<{ questionId: string, goodAnswer: string, givenAnswer: string },
    string,
    {
        state: AppState,
        extra: {
            questionGateway: QuestionGateway
        }
    }>(
    'questions/validateAnswer',
    async (givenAnswer, {
        getState,
        extra: {questionGateway}
    }) => {
        const questionId = getState().nextQuestionRetrieval.data!.id;
        const {goodAnswer} = await questionGateway.validateAnswer(questionId, givenAnswer);
        return {
            questionId,
            goodAnswer,
            givenAnswer,
        };
    });