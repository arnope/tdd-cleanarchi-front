import {createReducer} from "@reduxjs/toolkit";
import {retrieveNextQuestion} from "../usecases/next-question-retrieval/retrieveNextQuestion";
import {AppState} from "../store/appState";

export const nextQuestionRetrievalReducer =
    createReducer<AppState['nextQuestionRetrieval']>({
        data: null
    }, (builder) => {
        builder.addCase(retrieveNextQuestion.fulfilled, (state, {payload}) => {
            return {
                data: payload.nextQuestion
            }
        })
    });