import {createReducer} from "@reduxjs/toolkit";
import {AppState} from "../store/appState";
import {validateAnswer} from "../usecases/answer-validation/validateAnswer";

export const answerValidationReducer =
    createReducer<AppState['answerValidation']>(
        {}, (builder) => {
            builder.addCase(validateAnswer.fulfilled, (state, {payload}) => {
                return {
                    ...state,
                    [payload.questionId]: {
                        goodAnswer: payload.goodAnswer,
                        givenAnswer: payload.givenAnswer
                    }
                }
            })
        });