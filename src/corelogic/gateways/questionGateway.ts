import {Question} from "../models/question";

export interface QuestionGateway {
    retrieveNextOne(): Promise<Question | null>;
    validateAnswer(questionId: string, givenAnswer: string): Promise<{ goodAnswer: string}>;
}