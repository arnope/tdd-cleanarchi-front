export interface Question {
    id: string;
    label: string;
    choices: {[letter: string]: string}
}