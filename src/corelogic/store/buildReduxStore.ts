import {Action, configureStore, Store, ThunkDispatch} from "@reduxjs/toolkit";
import {AppState} from "./appState";
import {nextQuestionRetrievalReducer as nextQuestionRetrieval} from "../reducers/nextQuestionRetrieval.reducer";
import {answerValidationReducer as answerValidation} from "../reducers/answerValidation.reducer";
import {QuestionGateway} from "../gateways/questionGateway";
import {useDispatch} from "react-redux";

interface Dependencies {
    questionGateway: QuestionGateway
}

export const initReduxStore = (dependencies: Partial<Dependencies>) => {
    return configureStore({
            reducer: {
                nextQuestionRetrieval,
                answerValidation
            },
            devTools: true,
            middleware: getDefaultMiddleware =>
                getDefaultMiddleware({
                    thunk: {
                        extraArgument: dependencies
                    }
                })
        }
    );
}

export type ReduxStore = Store<AppState> & { dispatch: ThunkDispatch<AppState, Dependencies, Action> }
export const useAppDispatch = () => useDispatch<ThunkDispatch<AppState, Dependencies, Action>>()