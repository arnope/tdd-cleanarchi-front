import {Question} from "../models/question";

export interface AppState {
    nextQuestionRetrieval: {
        data: Question | null
    },
    answerValidation: {
        [questionId: string]: {
            goodAnswer: string,
            givenAnswer: string
        }
    }
}