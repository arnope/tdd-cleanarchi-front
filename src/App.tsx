import React from 'react';
import './App.css';
import {CurrentQuestion} from "./adapters/primary/react/components/currentQuestion.component";
import {MoneyPyramid} from "./adapters/primary/react/components/moneyPyramid.component";

export const App = () => {
    return <>
        <CurrentQuestion/>
        <div className="flex justify-center mt-20">
            <MoneyPyramid/>
        </div>
    </>
}