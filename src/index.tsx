import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import {App} from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import {FakeHttpQuestionGateway} from "./adapters/secondary/gateways/fakeHttpQuestionGateway";
import {RandomNumberGenerator} from "./adapters/secondary/gateways/randomNumberGenerator";
import {initReduxStore} from "./corelogic/store/buildReduxStore";

/*const questionGateway = new InMemoryQuestionGateway();
questionGateway.nextQuestion = "Qu'est-ce qu'un test unitaire ?";*/

const questionGateway = new FakeHttpQuestionGateway(new RandomNumberGenerator());
const store = initReduxStore({questionGateway});

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
      <Provider store={store}>
          <App />
      </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
