export interface NumberGenerator {
    until(n: number): number;
}