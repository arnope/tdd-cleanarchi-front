import {QuestionGateway} from "../../../corelogic/gateways/questionGateway";
import {Question} from "../../../corelogic/models/question";

export class InMemoryQuestionGateway implements QuestionGateway {

    private _nextQuestion: Question | null = null;

    retrieveNextOne(): Promise<Question | null> {
        return Promise.resolve(this._nextQuestion);
    }

    async validateAnswer(questionId: string, givenAnswer: string): Promise<{ goodAnswer: string }> {
        return Promise.resolve({goodAnswer: 'D'})
    }

    set nextQuestion(question: Question) {
        this._nextQuestion = question;
    }
}