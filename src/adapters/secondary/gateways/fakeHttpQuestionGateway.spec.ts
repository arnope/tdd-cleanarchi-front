import {FakeHttpQuestionGateway} from "./fakeHttpQuestionGateway";
import {DeterministicNumberGenerator} from "./deterministicNumberGenerator";
import {Question} from "../../../corelogic/models/question";

describe('Fake Http questions provider', () => {

    let numberGenerator: DeterministicNumberGenerator;

    beforeEach(() => {
       numberGenerator = new DeterministicNumberGenerator();
    });

    it('should pick a question from the pool', async () => {
        await expectPickedQuestion(0, {
            id: '123abc',
            label: "Qu'est-ce que le TDD ?",
            choices: {
                A: 'Toto-Driven Development',
                B: 'Trump-Driven Development',
                C: 'Test-Dodo Development',
                D: 'Test-Driven Development',
            }
        });
        await expectPickedQuestion(1, {
            id: '456def',
            label: "Qu'est-ce que React ?",
            choices: {
                A: 'Un framework Red',
                B: 'Un framework Web',
                C: 'Un framework Led',
                D: 'Un framework Ped',
            }
        });
    });

    const expectPickedQuestion = async (targetedIndex: number, expectedQuestion: Question) => {
        numberGenerator.expectedNumber = targetedIndex;
        expect(await new FakeHttpQuestionGateway(numberGenerator).retrieveNextOne()).toEqual(
            expectedQuestion
        )
    }

});