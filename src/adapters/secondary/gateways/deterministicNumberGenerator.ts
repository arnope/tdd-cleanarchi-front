import {NumberGenerator} from "./numberGenerator";

export class DeterministicNumberGenerator implements NumberGenerator {

    private _expectedNumber: number = -1;

    until(n: number): number {
        return this._expectedNumber;
    }

    set expectedNumber(value: number) {
        this._expectedNumber = value;
    }
}