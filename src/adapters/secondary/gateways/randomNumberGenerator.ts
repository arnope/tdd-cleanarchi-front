import {NumberGenerator} from "./numberGenerator";

export class RandomNumberGenerator implements NumberGenerator {
    until(n: number): number {
        return Math.floor(Math.random() * n);
    }
}