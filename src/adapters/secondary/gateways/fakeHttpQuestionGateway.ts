import {QuestionGateway} from "../../../corelogic/gateways/questionGateway";
import {NumberGenerator} from "./numberGenerator";
import {Question} from "../../../corelogic/models/question";

const questionsPool = [
    {
        id: '123abc',
        label: "Qu'est-ce que le TDD ?",
        choices: {
            A: 'Toto-Driven Development',
            B: 'Trump-Driven Development',
            C: 'Test-Dodo Development',
            D: 'Test-Driven Development',
        }
    },
    {
        id: '456def',
        label: "Qu'est-ce que React ?",
        choices: {
            A: 'Un framework Red',
            B: 'Un framework Web',
            C: 'Un framework Led',
            D: 'Un framework Ped',
        }
    }
];

export class FakeHttpQuestionGateway implements QuestionGateway {

    constructor(private numberGenerator: NumberGenerator) {
    }

    retrieveNextOne(): Promise<Question | null> {
        return Promise.resolve(questionsPool[this.numberGenerator.until(questionsPool.length)]);
    }

    async validateAnswer(questionId: string, givenAnswer: string): Promise<{ goodAnswer: string }> {
        if (questionId === '123abc')
            return {goodAnswer: "D"};
        return {goodAnswer: "B"};
    }
}