import {FunctionComponent} from "react";
import {useAppDispatch} from "../../../../corelogic/store/buildReduxStore";
import {validateAnswer} from "../../../../corelogic/usecases/answer-validation/validateAnswer";
import {AnswerVM} from "../selectors/answersVM";

interface Props {
    answerVM: AnswerVM
}

export const PossibleAnswer: FunctionComponent<Props> = ({answerVM}) => {

    const dispatch = useAppDispatch();

    const onClick = (letter: string) => (e: any) => {
        dispatch(validateAnswer(letter));
    }

    const answerBackground = {
        'REVEALED_RIGHT': 'bg-green-900',
        'REVEALED_WRONG': 'bg-orange-900',
        'NOT_REVEALED': 'bg-gray-900'
    }

    return <div className={"border-3 border-blue-300 rounded-lg px-3 py-1 " + answerBackground[answerVM.status]}
                data-testid={"click-answer-zone-" + answerVM.letter}
                onClick={onClick(answerVM.letter)}>
        <span className="text-orange-500">{answerVM.letter}</span>: {answerVM.label}
    </div>
}