import {render, waitFor, screen} from "@testing-library/react";
import {CurrentQuestion} from "./currentQuestion.component";
import {initReduxStore, ReduxStore} from "../../../../corelogic/store/buildReduxStore";
import {InMemoryQuestionGateway} from "../../../secondary/gateways/inMemoryQuestionGateway";
import {AppState} from "../../../../corelogic/store/appState";
import {Provider} from "react-redux";
import {selectAnswerVM} from "../selectors/answerVMSelector";
import {AnswersVM} from "../selectors/answersVM";

describe('Current question component', () => {

    let store: ReduxStore;
    let questionGateway: InMemoryQuestionGateway;
    let initialState: AppState;

    beforeEach(() => {
        questionGateway = new InMemoryQuestionGateway();
        questionGateway.nextQuestion = nextQuestion;
        store = initReduxStore({questionGateway});
        initialState = store.getState();
    });

    it('should retrieve the next question when component is loaded', async () => {
        render(<Provider store={store}>
            <CurrentQuestion/>
        </Provider>);
        await waitFor(() => expect(store.getState()).toEqual({
            ...initialState,
            nextQuestionRetrieval: {
                data: {
                    id: '123abc',
                    label: "Qu'est-ce que le TDD ?",
                    choices: {
                        A: 'Toto-Driven Development',
                        B: 'Trump-Driven Development',
                        C: 'Test-Dodo Development',
                        D: 'Test-Driven Development',
                    }
                }
            }
        }));
        expect(screen.getByTestId('question-title').textContent).toEqual("Qu'est-ce que le TDD ?");
    });

    it('should reveal the appropriate answers statuses', async () => {
        render(<Provider store={store}>
            <CurrentQuestion/>
        </Provider>);
        await expectAnswerRevealWhenClicking('A', {
            A: 'REVEALED_WRONG',
            B: 'NOT_REVEALED',
            C: 'NOT_REVEALED',
            D: 'REVEALED_RIGHT'
        });
        await expectAnswerRevealWhenClicking('D', {
            A: 'NOT_REVEALED',
            B: 'NOT_REVEALED',
            C: 'NOT_REVEALED',
            D: 'REVEALED_RIGHT'
        });
    });

    const expectAnswerRevealWhenClicking = async (givenAnswer: string, lettersWithAnswerStatuses: Record<string, string>) => {
        await waitFor(() => screen.getByTestId('click-answer-zone-' + givenAnswer).click())
        await waitFor(() => expect(selectAnswerVM(store.getState())).toEqual(
            {
                A: {
                    label: 'Toto-Driven Development',
                    letter: 'A',
                    status: lettersWithAnswerStatuses['A']
                },
                B: {
                    label: 'Trump-Driven Development',
                    letter: 'B',
                    status: lettersWithAnswerStatuses['B']
                },
                C: {
                    label: 'Test-Dodo Development',
                    letter: 'C',
                    status: lettersWithAnswerStatuses['C']
                },
                D: {
                    label: 'Test-Driven Development',
                    letter: 'D',
                    status: lettersWithAnswerStatuses['D']
                },
            }));
    }

    it('should answer to the right question', async () => {
        render(<Provider store={store}>
            <CurrentQuestion/>
        </Provider>);
        await waitFor(() => screen.getByTestId('click-answer-zone-D').click())
        await waitFor(() => expect(selectAnswerVM(store.getState())).toEqual({
            A: {
                label: 'Toto-Driven Development',
                letter: 'A',
                status: 'NOT_REVEALED'
            },
            B: {
                label: 'Trump-Driven Development',
                letter: 'B',
                status: 'NOT_REVEALED'
            },
            C: {
                label: 'Test-Dodo Development',
                letter: 'C',
                status: 'NOT_REVEALED'
            },
            D: {
                label: 'Test-Driven Development',
                letter: 'D',
                status: 'REVEALED_RIGHT'
            },
        }));
    });

    const nextQuestion = {
        id: '123abc',
        label: "Qu'est-ce que le TDD ?",
        choices: {
            A: 'Toto-Driven Development',
            B: 'Trump-Driven Development',
            C: 'Test-Dodo Development',
            D: 'Test-Driven Development',
        }
    };

});