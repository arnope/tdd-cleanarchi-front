import {Question} from "../../../../corelogic/models/question";
import {FunctionComponent} from "react";

interface Props {
    nextQuestion: Question | null
}

export const QuestionTitle: FunctionComponent<Props> = ({nextQuestion}) => {
    return <div data-testid='question-title' className="bg-gray-900 my-3 border-3 border-blue rounded-lg text-white p-2 text-center">
        {nextQuestion?.label}
    </div>
}