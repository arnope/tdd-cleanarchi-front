import {useSelector} from "react-redux";
import {useEffect} from "react";
import {useAppDispatch} from "../../../../corelogic/store/buildReduxStore";
import {retrieveNextQuestion} from "../../../../corelogic/usecases/next-question-retrieval/retrieveNextQuestion";
import {AppState} from "../../../../corelogic/store/appState";
import {QuestionTitle} from "./questionTitle.component";
import {PossibleAnswers} from "./possibleAnswers.component";

export const CurrentQuestion = () => {

    const dispatch = useAppDispatch();
    const nextQuestion = useSelector((state: AppState) => state.nextQuestionRetrieval.data);

    useEffect(() => {
        dispatch(retrieveNextQuestion());
    }, [dispatch]);

    return <div className="flex flex-row justify-center" >
        <div className="flex flex-col w-9/12">
            {nextQuestion && <div>
                <QuestionTitle nextQuestion={nextQuestion}/>
                <PossibleAnswers/>
            </div>}
        </div>
    </div>;
}