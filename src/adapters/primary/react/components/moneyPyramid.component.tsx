import {useSelector} from "react-redux";
import {selectMoneyEarned} from "../selectors/pyramid.selectors";
import {pyramid} from "../../../../corelogic/shortcut/pyramid";

export const MoneyPyramid = () => {

    const moneyEarned = useSelector(selectMoneyEarned);

    return <div className="h-100 w-20 bg-purple-500 text-center">
        {pyramid.map(money => {
            return <ul key={money}>
                <li className={moneyEarned === money ? 'font-bold' : ''}>{money}</li>
            </ul>
        })
        }
    </div>
}