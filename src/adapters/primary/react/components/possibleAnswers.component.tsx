import {PossibleAnswer} from "./possibleAnswer.component";
import {useSelector} from "react-redux";
import {selectAnswerVM} from "../selectors/answerVMSelector";

export const PossibleAnswers = () => {

    const answerVMs = useSelector(selectAnswerVM);

    return <div
        className="w-full justify-center grid grid-cols-2 text-white gap-4 font-mono text-sm text-left font-bold leading-6 bg-stripes-fuchsia rounded-lg">
        {Object.entries(answerVMs).map((letterWithAnswerVM, index) => {
            return <PossibleAnswer key={index} answerVM={letterWithAnswerVM[1]} />
        })
        }
    </div>;
}