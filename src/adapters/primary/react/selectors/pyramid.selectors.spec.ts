import {initReduxStore, ReduxStore} from "../../../../corelogic/store/buildReduxStore";
import {selectMoneyEarned} from "./pyramid.selectors";

describe('Pyramid selector', () => {

    let store: ReduxStore;

    beforeEach(() => {
        store = initReduxStore({});
    });

    describe('The game has not started yet', () => {

        it('should not make win money', () => {
            expect(selectMoneyEarned(store.getState())).toEqual(0);
        });

    });

    describe('Game has started', () => {

        beforeEach(() => {
            store.dispatch({
                type: 'questions/retrieveNextQuestion/fulfilled', payload: {
                    nextQuestion: {
                        id: '124abc',
                        label: "Qu'est-ce que le TDD ?",
                        choices: {
                            A: 'Toto-Driven Development',
                            B: 'Test-Driven Development',
                            C: 'Toutou-Driven Development'
                        }
                    }
                }
            });
        });

        it('no answer has been given yet', () => {
            expect(selectMoneyEarned(store.getState())).toEqual(0);
        });

        it('should increase my gain with one step each right answer', () => {
            store.dispatch({
                type: 'questions/validateAnswer/fulfilled', payload: {
                    questionId: '124abc',
                    givenAnswer: 'B',
                    goodAnswer: 'B',
                }
            });
            expect(selectMoneyEarned(store.getState())).toEqual(500);
        });

        it('should not increase when given answer is wrong', () => {
            store.dispatch({
                type: 'questions/validateAnswer/fulfilled', payload: {
                    questionId: '124abc',
                    givenAnswer: 'C',
                    goodAnswer: 'B',
                }
            });
            expect(selectMoneyEarned(store.getState())).toEqual(0);
        });

    });

});