import {AppState} from "../../../../corelogic/store/appState";
import {createSelector} from "reselect";
import {AnswersVM} from "./answersVM";

export const selectAnswerVM: (state: AppState) => AnswersVM = createSelector(
    (state: AppState) => state.nextQuestionRetrieval.data,
    (state: AppState) => state.answerValidation,
    (question, answerValidationState) => {
        const answerValidation = answerValidationState[question!.id];
        return Object.entries(question!.choices).reduce((acc, choice) => {
            const letter = choice[0];
            const label = choice[1];
            return {
                ...acc,
                [choice[0]]: {
                    letter,
                    label,
                    status: determineAnswerDisplay(answerValidation, letter)
                },
            }
        }, {});

        function determineAnswerDisplay(answerStatus: {
            goodAnswer: string,
            givenAnswer: string,
        }, currentAnswerLetter: string) {
            if (!answerStatus)
                return 'NOT_REVEALED'
            else if (answerValidation.goodAnswer === currentAnswerLetter)
                return 'REVEALED_RIGHT';
            else if (answerValidation.givenAnswer === currentAnswerLetter)
                return 'REVEALED_WRONG'
            else
                return 'NOT_REVEALED'

        }
    });