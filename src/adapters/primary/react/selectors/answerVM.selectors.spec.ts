import {selectAnswerVM} from "./answerVMSelector";
import {initReduxStore, ReduxStore} from "../../../../corelogic/store/buildReduxStore";
import {AnswersVM} from "./answersVM";

describe('View model generation for answers', () => {

    let store: ReduxStore;

    beforeEach(() => {
       store = initReduxStore({});
        store.dispatch({
            type: 'questions/retrieveNextQuestion/fulfilled', payload: {
                nextQuestion: {
                    id: '124abc',
                    label: "Qu'est-ce que le TDD ?",
                    choices: {
                        A: 'Toto-Driven Development',
                        B: 'Test-Driven Development',
                        C: 'Toutou-Driven Development'
                    }
                }
            }
        });
    });

    it('should not show any answer revealed', () => {
        expect(selectAnswerVM(store.getState())).toEqual({
            A: {
                letter: 'A',
                label: 'Toto-Driven Development',
                status: 'NOT_REVEALED'
            },
            B: {
                letter: 'B',
                label: 'Test-Driven Development',
                status: 'NOT_REVEALED'
            },
            C: {
                letter: 'C',
                label: 'Toutou-Driven Development',
                status: 'NOT_REVEALED'
            }
        } as AnswersVM)
    });

    it('should show your answer as revealed right', () => {
        store.dispatch({
            type: 'questions/validateAnswer/fulfilled', payload: {
                questionId: '124abc',
                givenAnswer: 'B',
                goodAnswer: 'B',
            }
        });

        expect(selectAnswerVM(store.getState())).toEqual({
            A: {
                letter: 'A',
                label: 'Toto-Driven Development',
                status: 'NOT_REVEALED'
            },
            B: {
                letter: 'B',
                label: 'Test-Driven Development',
                status: 'REVEALED_RIGHT'
            },
            C: {
                letter: 'C',
                label: 'Toutou-Driven Development',
                status: 'NOT_REVEALED'
            }
        } as AnswersVM)
    });

    it('should show your answer as revealed wrong', () => {
        store.dispatch({
            type: 'questions/validateAnswer/fulfilled', payload: {
                questionId: '124abc',
                givenAnswer: 'A',
                goodAnswer: 'B',
            }
        });

        expect(selectAnswerVM(store.getState())).toEqual({
            A: {
                letter: 'A',
                label: 'Toto-Driven Development',
                status: 'REVEALED_WRONG'
            },
            B: {
                letter: 'B',
                label: 'Test-Driven Development',
                status: 'REVEALED_RIGHT'
            },
            C: {
                letter: 'C',
                label: 'Toutou-Driven Development',
                status: 'NOT_REVEALED'
            }
        } as AnswersVM)
    });

});