export interface AnswersVM {
    [letter: string]: AnswerVM;
}

export interface AnswerVM {
    label: string;
    letter: string;
    status: AnswerStatus;
}

export type AnswerStatus = 'REVEALED_RIGHT' | 'REVEALED_WRONG' | 'NOT_REVEALED';