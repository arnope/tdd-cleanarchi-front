import {AppState} from "../../../../corelogic/store/appState";
import {pyramid} from "../../../../corelogic/shortcut/pyramid";
import {createSelector} from "reselect";

export const selectMoneyEarned = createSelector(
    (state: AppState) => state.nextQuestionRetrieval.data,
    (state: AppState) => state.answerValidation,
    (question, answerValidationState) => {
        const reversedPyramid = pyramid.slice().reverse();
        if (!question)
            return reversedPyramid[0];
        const answerValidation = answerValidationState[question.id];
        if (!answerValidation)
            return reversedPyramid[0];
        return answerValidation.givenAnswer === answerValidation.goodAnswer ? reversedPyramid[1] : reversedPyramid[0];
    });

